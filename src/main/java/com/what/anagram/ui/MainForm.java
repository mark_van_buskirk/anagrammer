package com.what.anagram.ui;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import org.controlsfx.control.RangeSlider;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import com.google.common.base.Strings;
import com.what.anagram.core.Anagrammer;
import com.what.anagram.core.Defaults;
import com.what.anagram.core.WordUtils;

public class MainForm extends Application {

	@FXML
	private ProgressIndicator indicator;

	@FXML
	private TextField inputTextField;

	@FXML
	private TextArea outputTextArea;

	@FXML
	private Slider sliderMaxWords;

	@FXML
	private TextField txtMaxAnagrams;

	@FXML
	private TextField txtIncludeWords;

	@FXML
	private TextField txtExcludeWords;

	@FXML
	private Button submitButton;

	@FXML
	private Button clearButton;

	@FXML
	private Button candidatesButton;

	@FXML
	private Label statusLabel;

	@FXML
	private GridPane gridPane;

	private RangeSlider wordLengthSlider = new RangeSlider(Defaults.MIN_LETTERS_PER_WORD, Defaults.MAX_LETTERS_PER_WORD,
			Defaults.MIN_LETTERS_PER_WORD, Defaults.MAX_LETTERS_PER_WORD);

	private Anagrammer anagrammer;
	private Thread processingThread;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws IOException {
		Font.loadFont(MainForm.class.getResource("/fonts/fontawesome-webfont.ttf").toExternalForm(), 18);

		anagrammer = Anagrammer.getFromFile(Defaults.DICTIONARY_PATH);

		FXMLLoader fxmlLoader = new FXMLLoader();
		fxmlLoader.setController(this);

		InputStream layout = MainForm.class.getClassLoader().getResourceAsStream("layout.fxml");
		Parent rootNode = fxmlLoader.load(layout);

		Scene scene = new Scene(rootNode, 880, 640);
		scene.getStylesheets().add("/css/custom.css");

		wordLengthSlider.setSnapToTicks(true);
		wordLengthSlider.setShowTickMarks(true);
		wordLengthSlider.setShowTickLabels(true);
		wordLengthSlider.setBlockIncrement(1);
		wordLengthSlider.setMajorTickUnit(2);
		wordLengthSlider.setMinorTickCount(1);
		wordLengthSlider.setTooltip(new Tooltip("Adjust the minimum and maximum number of letters for each word in an anagram."));
		gridPane.add(wordLengthSlider, 1, 4, 2, 1);

		addButtonStyles(submitButton);
		toggleSubmitButtonText(false);

		addButtonStyles(candidatesButton);

		addButtonStyles(clearButton);
		clearButton.setText("\uf1f8");

		inputTextField.requestFocus();
		inputTextField.selectAll();

		stage.setTitle("Anagrammer");
		stage.getIcons().add(new Image(MainForm.class.getResourceAsStream("/icon32.png")));
		stage.setScene(scene);
		stage.show();
	}

	private void addButtonStyles(Button button) {
		button.getStyleClass().add("icons");
		button.setStyle("-fx-font-size: 16;");
	}

	@FXML
	private synchronized void onClearPressed(ActionEvent event) {
		inputTextField.clear();
		txtExcludeWords.clear();
		txtIncludeWords.clear();
		outputTextArea.clear();
		statusLabel.setText(" ");

		sliderMaxWords.setValue(Defaults.MAX_WORDS_PER_ANAGRAM);
		txtMaxAnagrams.setText(Integer.toString(Defaults.MAX_ANAGRAMS));

		wordLengthSlider.setLowValue(wordLengthSlider.getMin());
		wordLengthSlider.setHighValue(wordLengthSlider.getMax());

		inputTextField.requestFocus();
	}

	@FXML
	private synchronized void onShowCandidatesPressed(ActionEvent event) {
		if (processingThread != null && processingThread.isAlive()) {
			cancelProcessing(event);
		} else {
			startAnagramGeneration(true);
		}
	}

	@FXML
	private synchronized void onSubmitPressed(ActionEvent event) {
		if (processingThread != null && processingThread.isAlive()) {
			cancelProcessing(event);
		} else {
			startAnagramGeneration(false);
		}
	}

	private void startAnagramGeneration(boolean showCandidates) {
		final String inputText = inputTextField.getText();
		if (Strings.isNullOrEmpty(inputText)) {
			return;
		}

		indicator.visibleProperty().set(true);

		final int maxWords = (int) sliderMaxWords.getValue();
		anagrammer.setMaxWordsPerAnagram(maxWords);

		final int minLetters = (int) wordLengthSlider.getLowValue();
		anagrammer.setMinLettersPerWord(minLetters);

		final int maxLetters = (int) wordLengthSlider.getHighValue();
		anagrammer.setMaxLettersPerWord(maxLetters);

		final int maxAnagrams = getIntFromTextField(txtMaxAnagrams, Defaults.MAX_ANAGRAMS);
		anagrammer.setMaxAnagrams(maxAnagrams);

		final Set<String> excludeWords = getUniqueStrings(txtExcludeWords);
		anagrammer.setExcludedWords(excludeWords);

		final Set<String> includeWords = getUniqueStrings(txtIncludeWords);
		anagrammer.setIncludedWords(includeWords);

		toggleSubmitButtonText(true);

		processingThread = new Thread(() -> {
			long startTime = System.nanoTime();

			List<String> results;
			final Comparator<String> comparator;
			if (showCandidates) {
				results = anagrammer.getCandidates(inputText);
				comparator = new WordLengthComparator();
			} else {
				results = anagrammer.getAnagrams(inputText);
				comparator = new AnagramComparator();
			}

			String outputText = "No results found.";
			int numResults = results.size();
			if (numResults > 0) {
				outputText = results.parallelStream().map(WordUtils::toTitleCase).sorted(comparator).collect(Collectors.joining("\n"));
			}

			final String textBoxContent = outputText;
			final long elapsedMillis = (System.nanoTime() - startTime) / 1000000;
			Platform.runLater(() -> {
				onProcessingComplete(textBoxContent, numResults, elapsedMillis);
			});
		});

		processingThread.setDaemon(true);
		processingThread.start();
	}

	private void onProcessingComplete(String textBoxContent, int numResults, long elapsedNanos) {
		outputTextArea.setText(textBoxContent);
		outputTextArea.positionCaret(0);

		Tooltip tooltip = outputTextArea.getTooltip();
		if (tooltip != null) {
			tooltip.setText(numResults + " results");
		}

		statusLabel.setText(numResults + " results, " + formatInterval(elapsedNanos));

		toggleSubmitButtonText(false);
		indicator.visibleProperty().set(false);
	}

	@FXML
	@SuppressWarnings("deprecation")
	private void cancelProcessing(ActionEvent event) {
		if (processingThread != null) {
			// This sort of sucks, but there's not an easy way to interrupt the call to generate the anagrams since the
			// generator itself is synchronous.

			processingThread.stop();
			processingThread = null;
		}

		toggleSubmitButtonText(false);
		indicator.visibleProperty().set(false);
		outputTextArea.setText("Operation was canceled.");
	}

	@FXML
	private void onEnterPressed(KeyEvent ke) {
		if (ke.getCode().equals(KeyCode.ENTER)) {
			onSubmitPressed(null);
		}
	}

	private void toggleSubmitButtonText(boolean isRunning) {
		String text;
		if (isRunning) {
			text = "\uf00d  Cancel";
		} else {
			text = "\uf002  Go";
		}
		submitButton.setText(text);
	}

	/**
	 * Retrieves an integer value from the provided textfield. If it could not be parsed, the textfield is updated to contain the provided
	 * default value, and the default value is returned.
	 */
	private static int getIntFromTextField(TextField txtField, int defaultValue) {
		Integer result;
		try {
			result = Integer.parseInt(txtField.getText());
		} catch (NumberFormatException ex) {
			txtField.setText(Integer.toString(defaultValue));
			result = defaultValue;
		}

		return result;
	}

	/**
	 * Retrieves a set of whitespace-delimited strings from a textfield.
	 */
	private static Set<String> getUniqueStrings(TextField txtField) {
		Set<String> wordSet = new HashSet<String>();
		String[] tokens = txtField.getText().split("\\s+");
		Collections.addAll(wordSet, tokens);
		return wordSet;
	}

	private static String formatInterval(final long elapsedMillis) {
		PeriodFormatter formatter = new PeriodFormatterBuilder().minimumPrintedDigits(2)
				.printZeroAlways()
				.appendMinutes()
				.appendSeparator(":")
				.appendSeconds()
				.appendSeparator(".")
				.appendMillis()
				.toFormatter();

		return formatter.print(new Period(elapsedMillis));
	}

}